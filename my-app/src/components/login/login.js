import React, { Component } from 'react';
import SignInComponent from '../login/signIn';
import SignUpComponent from '../login/signUp';

class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            signIn: true,
            signUp: false,
            SignInStatus:{
                borderBottom:'1px solid white',
                opacity:1
            },
            SignUpStatus:{
                borderBottom:'',
                opacity:.5
            }
        }
    }


    onclickSignUp = event => {
        this.setState(
            {
                signIn: false,
                signUp: true,
                SignInStatus:{
                    borderBottom:'',
                    opacity:.5
                },
                SignUpStatus:{
                    borderBottom:'1px solid white',
                    opacity:1
                }
            }
        )
    }

    onclickSignIn = event => {
        this.setState(
            {
                signIn: true,
                signUp: false,
                SignInStatus:{
                    borderBottom:'1px solid white',
                    opacity:1
                },
                SignUpStatus:{
                    borderBottom:'',
                    opacity:.5
                }
            }
        )
    }

    render() {

        return (
            <div>
                <div className="container">
                    <div className="CenterContainer">
                        <div className="SignTabs">
                            <button className="SignTabs-button" style={this.state.SignInStatus} onClick={this.onclickSignIn} >Sign In</button>
                            <button className="SignTabs-button" style={this.state.SignUpStatus} onClick={this.onclickSignUp}>Sign Up</button>
                        </div>
                        <div className="formsContainer">
                            {
                                this.state.signIn ?
                                    <SignInComponent />
                                    :
                                    <SignUpComponent/>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;