import React, { Component } from 'react';

class SignUp extends Component {

    render() {
        return (
            <form>
                <div className="Names-div">
                    <div className="first-name">
                        <label>First Name</label>
                        <input type="text" required></input>
                    </div>
                    <div className="last-name">
                        <label>Last Name</label>
                        <input type="text" required></input>
                    </div>
                </div>
                <div className="signUp-input">
                <label>Email</label>
                <input type="email" required></input>
                </div>
                <div className="signUp-input">                
                <label>Choose Password</label>
                <input type="password" required></input>
                </div>
                <div className="signUp-input">                
                <label>Confirm Password</label>
                <input type="password" required></input>
                </div>
                <button type="submitt" className="SignUp-button">SignUp</button>
            </form >
        );
    }
}

export default SignUp;